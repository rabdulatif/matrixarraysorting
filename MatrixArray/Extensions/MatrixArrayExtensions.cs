﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MatrixArray.Models;

namespace MatrixArray.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class MatrixArrayExtensions
    {

        /// <summary>
        /// Сортировка матрицу
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int[,] Sort(this int[,] array)
        {
            array.SortByRow();
            array.SortByColumn();

            return array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int[,] SortByRow(this int[,] array)
        {
            var columnLength = array.GetLength(0);
            var rowLength = array.GetLength(1);

            for (var columnIndex = 0; columnIndex < columnLength; columnIndex++)
                for (var rowCounter = 0; rowCounter < rowLength - 1; rowCounter++)
                    for (var rowIndex = 0; rowIndex < rowLength - 1; rowIndex++)
                    {
                        if (array[columnIndex, rowIndex] <= array[columnIndex, rowIndex + 1])
                            continue;

                        var temp = array[columnIndex, rowIndex + 1];
                        array[columnIndex, rowIndex + 1] = array[columnIndex, rowIndex];
                        array[columnIndex, rowIndex] = temp;
                    }

            return array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int[,] SortByColumn(this int[,] array)
        {
            var columnLength = array.GetLength(0);
            var rowLength = array.GetLength(1);

            for (var rowIndex = 0; rowIndex < rowLength; rowIndex++)
                for (var columnCounter = 0; columnCounter < columnLength - 1; columnCounter++)
                    for (int columnIndex = 0; columnIndex < columnLength - 1; columnIndex++)
                    {
                        if (array[columnIndex, rowIndex] <= array[columnIndex + 1, rowIndex])
                            continue;

                        var temp = array[columnIndex + 1, rowIndex];
                        array[columnIndex + 1, rowIndex] = array[columnIndex, rowIndex];
                        array[columnIndex, rowIndex] = temp;
                    }

            return array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int[,] IncreasingSort(this int[,] array)
        {
            var columnLength = array.GetLength(0);
            var rowLength = array.GetLength(1);

            int[] temp = new int[columnLength * rowLength];
            var counter = 0;

            array.Foreach((columnIndex, rowIndex) =>
            {
                temp[counter++] = array[columnIndex, rowIndex];
            });
            temp.BubbleSort();

            counter = 0;
            array.Foreach((columnIndex, rowIndex) =>
            {
                array[columnIndex, rowIndex] = temp[counter++];
            });

            return array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <param name="action"></param>
        public static void Foreach(this int[,] array, Action<int, int> action)
        {
            array.Foreach(0, 0, action);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <param name="columnStartIndex"></param>
        /// <param name="rowStartIndex"></param>
        /// <param name="action"></param>
        public static void Foreach(this int[,] array, int columnStartIndex, int rowStartIndex, Action<int, int> action)
        {
            var columnLength = array.GetLength(0);
            var rowLength = array.GetLength(1);

            for (var columnIndex = columnStartIndex; columnIndex < columnLength; columnIndex++)
            {
                var rowIndex = 0;
                if (columnIndex == columnStartIndex)
                    rowIndex = rowStartIndex;

                for (; rowIndex < rowLength; rowIndex++)
                    action.Invoke(columnIndex, rowIndex);
            }
        }
    }
}

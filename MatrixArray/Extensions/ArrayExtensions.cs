﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixArray.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ArrayExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int[] BubbleSort(this int[] array)
        {
            var temp = 0;

            for (var counter = 0; counter <= array.Length - 2; counter++)
                for (var rowCounter = 0; rowCounter <= array.Length - 2; rowCounter++)
                {
                    if (array[rowCounter] <= array[rowCounter + 1]) 
                        continue;
                    temp = array[rowCounter + 1];
                    array[rowCounter + 1] = array[rowCounter];
                    array[rowCounter] = temp;
                }

            return array;
        }
    }
}

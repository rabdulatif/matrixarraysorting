﻿using System;

namespace MatrixArray.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MatrixItemIndex
    {
        /// <summary>
        /// 
        /// </summary>
        public int ColumnIndex { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int RowIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="rowIndex"></param>
        /// <param name="value"></param>
        public MatrixItemIndex(int columnIndex, int rowIndex,int value)
        {
            ColumnIndex = columnIndex;
            RowIndex = rowIndex;
            Value = value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format(" [{0}, {1}]", ColumnIndex, RowIndex);
        }
    }
}

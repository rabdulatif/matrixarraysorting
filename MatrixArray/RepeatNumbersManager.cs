﻿using System;
using System.Collections.Generic;
using System.Linq;
using MatrixArray.Extensions;
using MatrixArray.Models;

namespace MatrixArray
{
    /// <summary>
    /// 
    /// </summary>
    public class RepeatNumbersManager
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, List<MatrixItemIndex>> _indexes;

        /// <summary>
        /// 
        /// </summary>
        private readonly int[,] _matrixArray;

        /// <summary>
        /// 
        /// </summary>
        private MatrixItemIndex _currentItemIndex;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="matrixArray"></param>
        public RepeatNumbersManager(int[,] matrixArray)
        {
            _matrixArray = matrixArray;
            _indexes = new Dictionary<int, List<MatrixItemIndex>>();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Build()
        {
            _matrixArray.Foreach(InternalBuild);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="rowIndex"></param>
        private void InternalBuild(int columnIndex, int rowIndex)
        {
            var value = _matrixArray[columnIndex, rowIndex];
            if (_indexes.ContainsKey(value))
                return;

            _currentItemIndex = new MatrixItemIndex(columnIndex, rowIndex, value);
            _matrixArray.Foreach(columnIndex, rowIndex + 1, InternalLoop);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="rowIndex"></param>
        private void InternalLoop(int columnIndex, int rowIndex)
        {
            var key = _matrixArray[columnIndex, rowIndex];
            if (_currentItemIndex.Value != key)
                return;

            if (!_indexes.ContainsKey(key))
            {
                _indexes.Add(key, new List<MatrixItemIndex>());
                _indexes[key].Add(_currentItemIndex);
            }

            _indexes[key].Add(new MatrixItemIndex(columnIndex, rowIndex, key));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Show()
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("-----Dublicate Elements-----");

            foreach (var indexInfo in _indexes)
            {
                Console.Write("Key: {0} -- ", indexInfo.Key);
                foreach (var index in indexInfo.Value)
                    Console.Write(index);

                Console.WriteLine();
            }
        }

    }
}

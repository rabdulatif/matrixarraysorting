﻿using System;
using MatrixArray.Extensions;
namespace MatrixArray
{
    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly int[,] MatrixArray = new int[5, 5];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            FillMatrixArray();
            SortMatrixArray();
            SumUncountableNumbers();
            RepeatNumbersIndexes();
            IncreasingSortMartixArray();

            Console.ReadLine();
        }

        /// <summary>
        /// 
        /// </summary>
        private static void FillMatrixArray()
        {
            var random = new Random();
            MatrixArray.Foreach((c, r) => MatrixArray[c, r] = random.Next(20));
            ShowMatrixArray();
        }

        /// <summary>
        /// 
        /// </summary>
        private static void SortMatrixArray()
        {
            MatrixArray.Sort();

            Console.WriteLine();
            Console.WriteLine();
            ShowMatrixArray();
        }

        /// <summary>
        /// 
        /// </summary>
        private static void IncreasingSortMartixArray()
        {
            MatrixArray.IncreasingSort();

            Console.WriteLine();
            Console.WriteLine();
            ShowMatrixArray();
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ShowMatrixArray()
        {
            var columnLength = MatrixArray.GetLength(0);
            var rowLength = MatrixArray.GetLength(1);

            for (var columnIndex = 0; columnIndex < columnLength; columnIndex++)
            {
                for (var rowIndex = 0; rowIndex < rowLength; rowIndex++)
                {
                    var value = MatrixArray[columnIndex, rowIndex];
                    Console.Write("{0}\t", value);
                }
                Console.WriteLine();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private static void SumUncountableNumbers()
        {
            var sum = 0;
            MatrixArray.Foreach((columnIndex, rowIndex) =>
            {
                var value = MatrixArray[columnIndex, rowIndex];
                if (value % 2 != 0)
                    sum += value;
            });

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Sum of Uncountable numbers: {0}", sum);
        }

        /// <summary>
        /// 
        /// </summary>
        private static void RepeatNumbersIndexes()
        {
            var manager = new RepeatNumbersManager(MatrixArray);
            manager.Build();
            manager.Show();
        }
    }
}
